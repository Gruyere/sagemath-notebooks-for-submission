#!/usr/bin/env python3

''' 
  This program is used in Appendix J.3 of the paper to prune potential
  ranges of the values p,q,r,s.
'''

from itertools import product
 
for (p,q,r,s) in product((-0.5, 0.5, 1.5), repeat=4):
  # By Equation (5)
  if p*(1-q)*(1-s) < 0 :
    continue
  # By Equation (6)
  if s*(1-r)*(1-p) < 0 :
    continue
  # By Equation (7)
  if q*r < 0 :
    continue
  # By Equation (8)
  if (1-p)*(1-s) < 0 :
    continue
  print((p,q,r,s))

